import React, {Component} from 'react';

class Header extends Component {
    render() {
        return (
            <header>
                <nav className="navbar navbar-dark bg-dark">
                    <a className="navbar-brand" href="#">
                        Todolist
                    </a>
                </nav>
            </header>
        );
    }
}

export default Header;