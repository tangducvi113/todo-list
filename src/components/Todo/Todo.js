import React, {Component} from 'react';
import {connect} from "react-redux";
import TodoItem from "./TodoItem";
import * as actions from '../../actions/Actions'

class Todo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            input : ''
        }
    }

    handleChange = (event) => {
        this.setState({
            input : event.target.value
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.addItem( this.state.input)
        this.setState({
            input : ''
        })
    }

    render() {
        const {
            input
        } = this.state;
        return (
            <div className="container">
                <div className="row">
                    <div className= "newTask col-3">
                        <form onSubmit = {this.handleSubmit}>
                            <div className="form-group">
                                <label>Add item</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    onChange = {this.handleChange}
                                    value = {input}
                                />
                            </div>

                            <button type="submit" className="btn-add" >Add</button>
                        </form>
                    </div>
                    <div className="col-9">
                        <div className="item">
                            <TodoItem />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addItem : (item) => {
            dispatch(actions.addItem(item))
        },
        editItem: (item) => {
            dispatch(actions.editItem(item))
        }
    }
}

export default connect(null, mapDispatchToProps)(Todo);