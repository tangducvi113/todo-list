import React, {Component} from 'react';
import {connect} from "react-redux";
import * as actions from '../../actions/Actions'

class TodoItem extends Component {
    delItem = (id) => {
        this.props.delItem(id)
    }

    editItem = (item) => {
        this.props.editItem(item)
    }
    render() {
        const {items} = this.props;

        return (
            <table className="table table-dark">
                <thead>
                    <tr>
                        <th scope="col">Date</th>
                        <th scope="col">Task</th>
                        <th scope="col">Event</th>
                    </tr>
                </thead>
                <tbody>
                    {items.map((item, index) => {
                        return (
                            <tr key = {index}>
                                <td></td>
                                <td>{item.task}</td>
                                <td>
                                    <button type="submit" className="btn-del" onClick = {() => this.delItem(item.id)}>Del</button>
                                    <button type="submit" className="btn-edit" onClick = {() => this.editItem(item)}>Edit</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        items : state.mang
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        delItem : (id) => {
            dispatch(actions.delItem(id))
        },

        editItem: (item) => {
            dispatch(actions.editItem(item))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TodoItem);