import { combineReducers } from 'redux';
import { v4 as uuidv4 } from 'uuid';
import {ADD_ITEM, DEL_ITEM, EDIT_ITEM} from "../constants/ActionTypes";
const initial = [
];

const mangReducer = (state = initial, action) => {
    switch(action.type) {
        case ADD_ITEM:
            const item = {
                id : uuidv4(),
                task : action.item
            }
            return [...state, item]
        case DEL_ITEM:
            return state.filter((item, index) => item.id !== action.id)
        case EDIT_ITEM:
            console.log(action)
            return state;
        default:
            return state;
    }
}

const myReducer = combineReducers({
    mang : mangReducer,
})

export default myReducer;
