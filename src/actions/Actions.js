import * as Types from '../constants/ActionTypes';

// export const add = item => dispatch => {
//     dispatch({
//         type : Types.ADD_ITEM,
//         item
//     });
// }

export const addItem = (item) => {
    return {
        type : Types.ADD_ITEM,
        item
    }
}

export const delItem = (id) => {
    return {
        type : Types.DEL_ITEM,
        id
    }
}

export const editItem = (item) => {
    return {
        type : Types.EDIT_ITEM,
        item
    }
}